# WebSocket Example with Spring Boot

This project demonstrates a simple implementation of WebSocket communication using Spring Boot, SockJS, and STOMP. It includes both the server-side (Spring Boot) and the client-side (HTML/JavaScript) components.

## Prerequisites

- Java 8 or higher
- Maven
- A modern web browser

## Getting Started

1. Clone the repository:

    ```
    git clone https://gitlab.com/techtalk5782345/websocket-example
    ```

2. Navigate to the project directory:

    ```
    cd websocket-example
    ```

3. Run the Spring Boot application:

    ```
    ./mvnw spring-boot:run
    ```

   This will start the WebSocket server at `http://localhost:8080/index.html`

4. Open ```src/main/resources/static/index.html``` in a web browser or deploy it to a web server.

5. Enter your name in the input field and click "Send" to see the WebSocket communication in action.

## Project Structure

- ```src/main/java/com/techtalk/websocketexample```: Contains the WebSocket controller and configuration classes.
- ```src/main/resources/static```: Contains the HTML page with JavaScript for the WebSocket client.

## WebSocket Server

- The server is implemented using Spring Boot.
- It handles messages with the destination ```/hello``` and responds by sending greetings to the topic ```/topic/greetings```.

## WebSocket Client

- The client is a simple HTML page with JavaScript.
- It establishes a WebSocket connection to ```/ws``` and subscribes to the topic ```/topic/greetings```.
- Users can enter their names, click "Send," and receive greetings from the server.

